<?php

if( $_GET['id'] )
$char = db()->splitter_character->get("id = %d", $_GET['id'])->object('splitter_character');
if( empty( $char ))
throw new Exception('kaputt');

global $elementTypes;
require 'inc/load_data.php';

foreach( $elementTypes as $t )
$char->{$t} = json_decode( $char->{$t.'_selections'}, true);

$data = array(
		'definitions' => $typeDefinitions,
		'char' => $char,
);

file_put_contents(
		"cache/fdf/char_{$char->id}.fdf",
		utf8_decode(template('creation/fdf')->render($data))
);

system("/usr/bin/pdftk data/pdf/Splittermond_Charakterbogen_Formular.pdf fill_form cache/fdf/char_{$char->id}.fdf output cache/pdf/char_{$char->id}.pdf flatten");
header("LOCATION: cache/pdf/char_{$char->id}.pdf");

