<?php

$query = "SELECT se.id, se.name, count(sc.id) c
	FROM `splitter_character` sc
	JOIN splitter_element se ON sc.%s = se.id
	WHERE sc.create_by != 0
	GROUP BY se.id, se.name
	ORDER BY c DESC";

$tbl = new list_sql(MODUL_SELF,null,5);
$tbl->text('Name', 'name');
$tbl->text('Benutzungen', 'c');

foreach( $elementTypes as $t ) {
	$tbl->prefix = $t;
	$view->box($tbl->get(sprintf( $query, $t)), ucfirst($t));
}
