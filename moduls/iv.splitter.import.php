<?php

if( !empty( $_FILES['data']['tmp_name'] )) {
	try {
		require 'inc/load_data.php';
		$definitions = new splitter_definitions($typeDefinitions);
		$elementType = $_POST['type'];

		switch($elementType) {
			case 'rasse': $definition = $definitions->getRasse(); break;
			case 'herkunft': $definition = $definitions->getKultur(); break;
			case 'abstammung': $definition = $definitions->getAbstammung(); break;
			case 'ausbildung': $definition = $definitions->getAusbildung(); break;
			default: throw new Exception('Invalid Type');
		}

		$content = file_get_contents( $_FILES['data']['tmp_name'] );
		$data = json_decode( $content, true );
		$elements = array();

		if( !is_array( $data ))
			throw new Exception('Invalides JSON: '.json_last_error_msg());

		foreach( $data as $row ) {
			if( empty( $row['name'] ) || empty( $row['stats'] ))
				throw new Exception('Ungültiges Element: '.json_encode( $element ));

			$element = new splitter_element( $definition, $typeDefinitions);
			foreach( $row['stats'] as $type => $stats )
				foreach( $stats as $stat )
					$element->add($type, $stat['options'], $stat['value']);
			$element->validate();

			$element->name = $row['name'];
			$element->description = $row['description'];
			$elements[] = $element;
		}

		foreach( $elements as $row ) {
			$id = db()->query(
					"SELECT id FROM splitter_element WHERE name = '%s' AND type = '%s' AND create_by = %d",
					$row->name, $elementType, current_user())->value();

			$query = array(
					'type' => $elementType,
					'name' => $row->name,
					'description' => $row->description,
					'stats' => $row
			);

			if( empty( $id )) db()->splitter_element->insert($query);
			elseif( empty( $_POST['skip'] )) db()->splitter_element->updateRow($query, $id);
			elseif( $_POST['skip'] == 2 ) throw new Exception("Eintrag für {$row->name} bereits vorhanden.");

			$log[] = "Importiere {$row->name}... erfolgreich";
		}

		$view->success('Import erfolgreich');
		$view->box(implode('<br>', $log), "Import Log");
	} catch(Exception $e ) {
		$view->error($e->getMessage());
	}
}

if( empty( $_POST['export'])) {

	$types = array_combine($elementTypes,$elementTypes);

	$form = new form(MODUL_SELF);
	$form->enctype = 'multipart/form-data';
	$form->text('data', 'Json-Datei')->input('type', 'file');
	$form->select('type', 'Element-Typ', $types);
	$form->select('skip', 'Existierende Datensätze', array('Überschreiben', 'Überspringen', 'Import Abbrechen'));
	$view->box($form, 'Datei Importieren');


	$form = new form(MODUL_SELF);
	$form->select('export', 'Element-Typ', $types);
	$view->box($form, 'Daten Exportieren');

} else {
	header('Content-Type: application/json');
	$view->format = 'plain';

	$data = db()->query(
			"SELECT name, description, stats FROM splitter_element WHERE type = '%s' AND create_by = %d",
			$_POST['export'], current_user())->assocs();
	foreach( $data as &$e ) $e['stats'] = json_decode($e['stats'], true);
	$view->content(json_encode($data, JSON_PRETTY_PRINT));
}
