<?php

$view->js( 'http://bar.iv-dev.de/icons.js' );

if( !empty( $_POST['src'] )) {
	$dst = empty( $_POST['dst'] ) ? $_POST['src'] : $_POST['dst'];
	file_put_contents( "assets/large/$dst.png", file_get_contents( "http://bar.iv-dev.de/big/{$_POST['src']}.png" ));
	file_put_contents( "assets/small/$dst.png", file_get_contents( "http://bar.iv-dev.de/icons/{$_POST['src']}.png" ));
	$view->box( "Icon {$_POST['src']} wurde geladen", "Erfolg" );
}

$form = new form( MODUL_SELF );
$form->text( 'src', 'Quell-Datei' );
$form->text( 'dst', 'Ziel-Datei' );
$view->box( $form, 'Iconloader' );

$view->box( '<img width="700" onclick="$(\'input[name=src]\').val( function() { var i = icons[Math.floor(event.layerX/20)+Math.floor(event.layerY/20)*35]; return i.substring( 6, i.length-4 ); });" alt="iconmap" style="position: relative;" src="http://bar.iv-dev.de/iconmap.png">', 'Iconliste' );