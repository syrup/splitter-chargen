<?php

if( !empty( $_GET['publish'] )) {
	db()->splitter_element->update(array('create_by'=>0), "id = %d AND create_by != %d", $_GET['publish'], current_user());
	throw new redirect( MODUL_SELF );
}

$users = db()->user_data->all()->relate();
$users[0] = 'Öffentlich';
$types = array_combine($elementTypes,$elementTypes);
$types[''] = 'Alle';

$rc =  new data_controller( 'splitter_element', MODUL_SELF );
$rc->add( 'id', 'Id', 1, 0, 0, 0 );
$rc->add( 'name', 'Name' );
$rc->add( 'description', 'Beschreibung', 0, 0, 1, 0, 'textarea' );
$rc->add( 'type', 'Typ' );
$rc->add( 'stats', 'Werte', 0, 0, 1, 1, 'textarea' );
$rc->add( 'create_by', 'Autor', 1, 0, 1, 0, 'select', $users );
$rc->add( 'public', 'Shared', 1, 0, 1, 0, 'select', array('nein', 'ja'));
$rc->edit = $modulrights['edit'];
$rc->delete = $modulrights['delete'];
$rc->create = false;


$ep = db()->query("SELECT layer FROM content_panel WHERE script = 'splitter.element'")->value();

$rc->option('assets/small/information.png','id','View');
$rc->option('assets/small/wrench.png','id','Bearbeiten', 'index.php?page='.$ep );

$intops = array('create_by', 'public');
if( isset( $_POST['search'] )) {
	foreach( $_POST['search'] as $key => $value )
		if( in_array($key, $intops) && $value != '' ) $rc->condition .= db()->format(" AND `%s` = %d", $key, $value );
		elseif( !empty( $value )) $rc->condition .= db()->format(" AND `%s` LIKE '%s%%'", $key, $value );
}

if( $modulrights['profil'])
	$rc->option('assets/small/user_information.png', 'profil', 'Profil');

if( $rc->run()) throw new redirect( MODUL_SELF );

$users[''] = 'Alle';
$searchForm = new form(MODUL_SELF);
$searchForm->text('search[id]', 'ID', $_POST['search']['id'] );
$searchForm->text('search[name]', 'Name', $_POST['search']['name'] );
$searchForm->select('search[type]', 'Type', $types, $_POST['search']['type'] );
$searchForm->select('search[create_by]', 'Autor', $users, $_POST['search']['create_by'] );
$searchForm->select('search[public]', 'Shared', array('' => 'Alle', '0' => 'Nein', '1' => 'Ja'), $_POST['search']['public'] );

if( !empty( $_GET['id'] )) {
	$ele = db()->splitter_element->row($_GET['id'] )->assoc();
	$tbl = new list_array(MODUL_SELF);
	$tbl->add(new list_column_implode('Type','options',' oder '));
	$tbl->num('Wert','value');

	$view->box(htmlspecialchars($ele['description']), $ele['name']);

	foreach( json_decode($ele['stats'], true) as $type => $stats )
		$view->box($tbl->get($stats),$captions[$type]);

	$link = new html_link(MODUL_SELF);
	$link->class = 'btn';

	$buttons  = $link->get('Zurück').' ';
	$buttons .= $link->get('Veröffentlichen', array('publish'=>$ele['id'])).' ';
	$view->box($buttons,'Aktionen');
} elseif( !empty(  $_GET['edit'] )) {
	$view->box(  $rc->get_edit( $_GET['edit'] ),'Element bearbeiten');
} else {
	$view->box( $searchForm, 'Elemente suchen' );
	$view->box( $rc->get_list(), 'Elemente verwalten' );
}
