<?php

class splitter_controller_create {
	/** @var splitter_character */
	private $character;

	function __construct(splitter_character $character) {
		$this->character = $character;
	}

	/**
	 * @param $elementTypes
	 * @param $step
	 * @param $typeDefinitions
	 * @return array
	 * @throws redirect
	 */
	public function selectElement($elementTypes, $step, $typeDefinitions) {
		$templateData = array();
		$templateData['type']  = $type = $elementTypes[$step - 1];
		$templateData['title'] = ucfirst($type);

		if (empty($_GET['element'])) {
			$templateData['elementpage'] = db()->query("
				SELECT layer FROM content_panel WHERE script = 'splitter.element'")->value();
			$templateData['elements']    = array(
					'system'    => db()->query(
							"SELECT id, name, description FROM splitter_element WHERE type = '%s' AND create_by = 0 ORDER BY name",
							$type)->assocs(),
					'own'       => db()->query(
							"SELECT id, name, description FROM splitter_element WHERE type = '%s' AND create_by = %d ORDER BY name",
							$type, current_user())->assocs(),
					'community' => db()->query(
							"SELECT id, name, description FROM splitter_element WHERE type = '%s' AND create_by != %d AND public ORDER BY name",
							$type, current_user())->assocs());
		} else {
			$element = db()->splitter_element
					->get("type = '%s' AND (create_by = %d OR create_by = 0 OR public) AND id = %d", $type, current_user(), $_GET['element'])
					->object('splitter_element', array(array(), $typeDefinitions));

			if (isset($_POST['continue'])) {
				$this->character->setElement($element, $_POST['values']);
				saveCharacter($this->character);
			}

			$templateData['element'] = $element;
		}

		return $templateData;
	}

	/**
	 * @param $atr
	 * @return array
	 * @throws Exception
	 * @throws redirect
	 */
	public function selectAttributes($atr) {
		$templateData = array();
		$templateData['attributes'] = $atr;

		if (!empty($_POST['attributes'])) {
			$attrs = array_intersect_key($_POST['attributes'], $atr);

			try {
				$this->character->setAttributes($attrs);
				saveCharacter($this->character);
			} catch (redirect $e) {
				throw $e;
			} catch (Exception $e) {
				$templateData['values'] = $attrs;
				$templateData['error']  = $e->getMessage();
			}
		}

		return $templateData;
	}

	/**
	 * @param $typeDefinitions
	 * @return array
	 * @throws Exception
	 * @throws redirect
	 */
	public function selectFree($typeDefinitions) {
		$templateData = array();
		$definitions = new splitter_definitions($typeDefinitions);
		$definition  = $definitions->getFree($this->character);

		$templateData['stats'] = $definition;
		$templateData['post']  = $_POST;

		if (!empty($_POST)) {
			$types  = $_POST['types'];
			$values = $_POST['values'];

			try {
				$element = new splitter_element($definition, $typeDefinitions);

				foreach ($definition as $type => $data)
					if (is_array($types[$type]))
						foreach ($types[$type] as $k => $row)
							if (!empty($row))
								$element->add($type, $row, $values[$type][$k]);

				$element->validate();
				$element->type = 'free';

				$this->character->setElement($element);
				$this->character->validate();
				saveCharacter($this->character);
			} catch (redirect $e) {
				throw $e;
			} catch (Exception $e) {
				$templateData['error'] = $e->getMessage();
			}
		}

		return $templateData;
	}

	/**
	 * @param $mnz
	 * @param $wks
	 * @return array
	 * @throws redirect
	 */
	public function selectMondzeichen($mnz, $wks) {
		$templateData = array();
		$templateData['mondzeichen'] = $mnz;
		$wksValues            = array();
		foreach ($wks as $k => $v) $wksValues[$k] = $k;
		$templateData['stats'] = array('weaknesses' => array('title' => 'Schwächen', 'values' => $wksValues, 'min' => 1, 'max' => 1));

		if (!empty($_POST['mondzeichen'])) {
			$weaknesses = array();

			foreach ($_POST['types']['weaknesses'] as $w)
				if (!in_array($w[0], $weaknesses))
					$weaknesses[$w[0]] = 1;

			$this->character->setMondzeichen($_POST['mondzeichen'], $weaknesses);
			saveCharacter($this->character);
		}

		return $templateData;
	}

	/**
	 * @param $zauber
	 * @param $mst
	 * @param $skl
	 * @return array
	 * @throws Exception
	 * @throws redirect
	 */
	public function finalize($zauber, $mst, $skl) {
		$templateData = array();
		$freeMasteries = array();
		$freeSpells    = array();

		$magieschulen = array();
		foreach ($zauber as $id => $z)
			foreach ($z['schulen'] as $s => $g)
				$magieschulen[$s][$g][] = $id;

		foreach ($this->character->stats['skills'] as $skill => $level) {
			if ($level > 5) {
				$optionen = array();
				foreach ($mst[$skill]['Schwerpunkte'] as $s) $optionen[$s['id']] = $s['id'];
				foreach ($mst[$skill][1] as $s) $optionen[$s['id']] = $s['id'];
				$freeMasteries[$skill] = array_diff($optionen, array_keys($this->character->stats['masteries']));
			}

			if ($skl[$skill]['type'] == 'magie') {
				$grad               = floor($level / 3);
				$freeSpells[$skill] = array();
				foreach ($magieschulen[$skill] as $g => $spells)
					if ($g <= $grad)
						$freeSpells[$skill][$g] = $spells;
				ksort($freeSpells[$skill]);
			}
		}

		if (!empty($_POST['equipment'])) {
			if (is_array($_POST['masterie'])) {
				foreach ($_POST['masterie'] as $skill => $m) {
					if (!in_array($m, $freeMasteries[$skill]))
						throw new Exception('Ungültige Meisterschaft: ' . $m);
					$this->character->stats['masteries'][$m] = 1;
				}
			}

			if (is_array($_POST['spell'])) {
				$this->character->stats['spells'] = array();

				foreach ($_POST['spell'] as $skill => $grad)
					foreach ($grad as $g => $s) {
						if (!in_array($s, $freeSpells[$skill][$g]))
							throw new Exception('Ungültige Zauber: ' . $g);
						$this->character->stats['spells'][$skill][$s] = 1;
					}
			}

			$this->character->inventar = $_POST['equipment'];
			saveCharacter($this->character);
		}

		$templateData['masteries'] = $freeMasteries;
		$templateData['spells']    = $freeSpells;
		return $templateData;
	}
} 
