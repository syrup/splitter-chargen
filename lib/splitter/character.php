<?php

class splitter_character {
	public $id;
	public $name;

	public $rasse;
	public $herkunft;
	public $abstammung;
	public $ausbildung;

	public $start_attribute;
	public $free_selections;
	public $mondzeichen;
	public $inventar;
	public $stats;

	private $orig;

	public function __construct() {
		$this->stats = $this->orig = json_decode($this->stats, true);
		$this->start_attribute = json_decode($this->start_attribute, true);
	}

	public function getGeschwindigkeit() {
		return $this->stats['misc']['size'] + $this->stats['sttributes']['BEW'];
	}

	public function getInitiative() {
		return 10 - $this->stats['attributes']['INT'];
	}

	public function getLebenspunkte() {
		return $this->stats['misc']['size'] + $this->stats['attributes']['KON'];
	}

	public function getFokus() {
		return 2 *( $this->stats['attributes']['MYS'] + $this->stats['attributes']['WIL'] );
	}

	public function getVerteidigung() {
		$wert = 12 + $this->stats['attributes']['BEW'] + $this->stats['attributes']['STÄ'] + ( 5 - $this->stats['misc']['size'] ) * 2;
		return $wert + ($this->getHeldengrad()-1)*2;
	}

	public function getGeistigerWiederstand() {
		$wert = 12 + $this->stats['attributes']['VER'] + $this->stats['attributes']['WIL'];
		return $wert + ($this->getHeldengrad()-1)*2;
	}

	public function getKoerperlichenWiederstand() {
		$wert = 12 + $this->stats['attributes']['KON'] + $this->stats['attributes']['WIL'];
		return $wert + ($this->getHeldengrad()-1)*2;
	}

	public function getAvailableEp() {
		return $this->stats['ep']['total'] - $this->stats['ep']['used'];
	}

	public function getSplitterPunkte() {
		return 2+$this->getHeldengrad();
	}

	public function getHeldengrad() {
		$used = $this->stats['ep']['used'];
		if( $used >= 600 ) return 4;
		elseif( $used >= 300 ) return 3;
		elseif( $used >= 100 ) return 2;
		else return 1;
	}

	public function getMaxAttribut($attr) {
		// @TODO Rassenmodifikator einrechnen
		return $this->start_attribute[$attr] + $this->getHeldengrad();
	}

	public function getMaxSkill() {
		return $this->getHeldengrad()*3+3;
	}

	/**
	 * Get current character generation step
	 * @return int
	 */
	public function getStep() {
		global $elementTypes;

		foreach( $elementTypes as $i => $t)
			if( empty( $this->{$t} ))
				return $i+1;

		if( empty( $this->start_attribute ))
			return 5;
		if( empty( $this->free_selections ))
			return 6;
		if( empty( $this->mondzeichen ))
			return 7;
		if( empty( $this->inventar ))
			return 8;

		return 9;
	}

	/**
	 * @param $element
	 * @param $selection
	 * @throws Exception
	 * @todo doppelte särken oder meisterschaften müssen später neu gewählt werden können
	 */
	public function setElement(splitter_element $element, $selection = array()) {
		if( !empty( $this->{$element->type} ))
			throw new Exception("{$element->type} bereits gewählt!");

		$stats = $element->select( $selection );

		foreach( $stats as $statType => $values ) {
			if( !isset( $this->stats[$statType] ))
				$this->stats[$statType] = array();
			foreach( $values as $attr => $v )
				$this->stats[$statType][$attr] += $v;
		}

		$stats['name'] = $element->name;

		$this->{$element->type} = $element->id;
		$this->{$element->type.'_selections'} = json_encode($stats);
	}

	/**
	 * @param $attrs
	 * @throws Exception
	 */
	public function setAttributes($attrs) {
		$sum = array_sum($attrs);
		$this->start_attribute = $attrs;

		if( $sum > 18 )
			throw new Exception('Zu viele Punkte vergeben.');
		if( $sum < 18 )
			throw new Exception('Zu wenig Punkte vergeben.');

		foreach( $attrs as $a => $v ) {
			if( $v < 1 || $v > 3 )
				throw new Exception('Invalid Value for '.$a);
			$this->stats['attributes'][$a] += $v;
		}
	}

	public function setMondzeichen( $zeichen, $weaknesses ) {
		$this->mondzeichen = $zeichen;
		$this->stats['weaknesses'] = $weaknesses;
	}

	public function validate() {
		// keine doppelten stärken (zumindest die die man nicht doppelt haben darf)

		foreach( $this->stats['skills'] as $s => $l )
			if( $l > 6 ) throw new Exception("Skill $s darf maximal 6 betragen!");
	}

	public function getDiff() {
		return getDiff( $this->orig, $this->stats );
	}
}
