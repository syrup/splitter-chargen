<?php


class splitter_element {
	private $definition;
	private $typeDefinitions;


	public $id;
	public $type;
	public $name;
	public $description;
	public $public;
	private $stats;

	function __construct($definition, $typeDefinitions) {
		if( empty( $this->stats ))
			foreach( $definition as $type => &$data ) {
				$data['current'] = 0;
				$data['already'] = array();
				$this->stats[$type] = array();
			}
		else
			$this->stats = json_decode($this->stats, true);

		$this->definition = $definition;
		$this->typeDefinitions = $typeDefinitions;
	}

	/**
	 * @param $type
	 * @param $options
	 * @param $value
	 * @throws Exception
	 */
	public function add( $type, $options, $value ) {
		if( $type == 'strengths' ) $value = 1;
		$def = $this->definition[$type];
		$cost = $this->getCosts( $type, $options, $value );

		foreach( $options as $o )
			if( empty( $def['values'][$o] ))
				throw new Exception("Invalide Auswahl '$o' für {$def['title']}");

		$this->validateRange($value, $def);
		if( $type != 'attributes' ) $this->validateUnique($options, $def);
		if( $type == 'skills' ) $this->validateSkill($type, $options, $value, $def);

		$this->definition[$type]['current'] += $cost;
		$this->definition[$type]['already'] = array_merge( $def['already'], $options );

		$this->stats[$type][] = array(
			'options' => $options,
			'value' => intval( $value ),
		);
	}

	/**
	 * @return string
	 */
	function __toString() {
		if( defined('JSON_PRETTY_PRINT')) return json_encode( $this->stats, JSON_PRETTY_PRINT );
		else return json_encode( $this->stats );
	}

	/**
	 * @throws Exception
	 */
	public function validate() {
		foreach( $this->definition as $type => $def ) {
			if( $def['total_magic'] && $def['magic'] > $def['total_magic'] )
				throw new Exception("Zu viele Magieschulen vergeben: {$def['magic']}!");
			if( $def['current'] > $def['total'] )
				throw new Exception("Zu viele {$def['title']} vergeben: {$def['current']}!");
			if( $def['current'] < $def['total'] && $type != 'magic' )
				throw new Exception("Zu wenig {$def['title']} vergeben: {$def['current']}!");
		}
	}

	/**
	 * @param $type
	 * @param $options
	 * @param $value
	 * @return mixed
	 * @throws Exception
	 */
	private function getCosts( $type, $options, $value) {
		if( $type != 'strengths' ) return $value;
		$typeDef = $this->typeDefinitions['strengths'];
		$points = $typeDef[$options[0]]['points'];

		// calculate total strendths
		foreach ($options as $o)
			if ($points != $typeDef[$o]['points']) {
				throw new Exception('Unterschiedlich teure Stärken können nicht kombiniert werden');
			}

		return $points * $value;
	}

	/**
	 * @param $value
	 * @param $def
	 * @throws Exception
	 */
	private function validateRange($value, $def) {
		if ($value > $def['max'])
			throw new Exception("Der Wert der {$def['title']} darf maximal {$def['max']} betragen!");
		if ($value < $def['min'])
			throw new Exception("Der Wert der {$def['title']} darf minimal {$def['max']} betragen!");
	}

	/**
	 * @param $options
	 * @param $def
	 * @throws Exception
	 */
	private function validateUnique($options, $def) {
		foreach ($options as $s)
			if (in_array($s, $def['already']))
				throw new Exception("{$def['title']} dürfen nicht mehrfach gewählt werden.");
	}

	/**
	 * @param $type
	 * @param $options
	 * @param $value
	 * @param $def
	 * @throws Exception
	 */
	public function validateSkill($type, $options, $value, $def) {
		$typeDef = $this->typeDefinitions['skills'];
		$includeMagic = false;

		foreach ($options as $o) {
			$optType = $typeDef[$o]['type'];

			if ($optType == 'magie') {
				$includeMagic = true;
				if ($def['max_magic'] && $value > $def['max_magic'])
					throw new Exception("Zu hoher Wert für Magieschule: $o");
			} elseif ($def['max_skill'] && $value > $def['max_skill'])
				throw new Exception("Zu hoher Wert für Fertigkeit: $o");
		}

		if ($includeMagic) $this->definition[$type]['magic']++;
	}

	/**
	 * Getter for the stats
	 * @return mixed
	 */
	public function getStats() {
		return $this->stats;
	}

	/**
	 * Converts element back to post data for editing
	 * @return array
	 */
	public function getPostValues() {
		$values = array(
				'name' => $this->name,
				'description' => $this->description,
				'public' => $this->public,
				'types' => array(),
				'values' => array());

		foreach( $this->stats as $type => $data ) {
			$values['types'][$type] = array();
			$values['values'][$type] = array();

			foreach( $data as $stat ) {
				$values['types'][$type][] = $stat['options'];
				$values['values'][$type][] = $stat['value'];
			}
		}

		return $values;
	}

	/**
	 * Get the stats for a specific selection
	 * @param $selection
	 * @throws Exception
	 * @return array
	 */
	public function select( $selection ) {
		$result = array();

		foreach( $this->stats as $statType => $values ) {
			if( !isset( $result[$statType] ))
				$result[$statType] = array();

			$vals = $selection[$statType];

			foreach( $values as $i => $v ) {
				if( count( $v['options'] ) > 1 ) {
					$attr = $vals[$i];
					if( !in_array( $attr, $v['options'] ))
						throw new Exception('Evil manipulation!');
				} else $attr = $v['options'][0];

				$result[$statType][$attr] = $v['value'];
			}
		}

		return $result;
	}
}
