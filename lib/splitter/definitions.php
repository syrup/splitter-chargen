<?php


class splitter_definitions {
	private $typeDefinitions;

	function __construct($typeDefinitions) {
		$this->typeDefinitions = $typeDefinitions;
	}

	public static function getSelection( $typeDef ) {
		$values = array();
		foreach( $typeDef as $k => $v ) $values[$k] = $k;
		return $values;
	}

	public static function getSelectionNames( $typeDef ) {
		$values = array();
		foreach( $typeDef as $k => $v ) $values[$k] = $v['name'];
		return $values;
	}

	public static function getSelectionCosts( $typeDef ) {
		$values = array();
		foreach( $typeDef as $k => $v ) $values[$k] = $k.' ('.$v['points'].')';
		return $values;
	}

	public static function getSelectionNoMagic( $typeDef ) {
		$values = array();
		foreach( $typeDef as $k => $v ) if( $v['type'] != 'magie' ) $values[$k] = $k;
		return $values;
	}

	public static function getSelectionMasteries( $typeDef ) {
		$values = array();
		foreach( $typeDef as $k => $schwellen ) {
			foreach( $schwellen['Schwerpunkte'] as $s ) $values[$s['id']] = $s['id'];
			foreach( $schwellen[1] as $s ) $values[$s['id']] = $s['id'];
		}
		return $values;
	}

	public static function getCosts( $typeDef ) {
		$values = array();
		foreach( $typeDef as $k => $v ) $values[$k] = $v['points'];
		return $values;
	}

	public function getFree(splitter_character $char) {
		$definition = array();
		$definition['strengths'] = array( 'title' => 'Stärken', 'values' => $this->getSelectionCosts($this->typeDefinitions['strengths']),
				'min' => 1, 'max' => 1, 'total' => 3 + $char->stats['misc']['strengths'],
				'costs' => $this->getCosts($this->typeDefinitions['strengths']));
		$definition['skills'] = array( 'title' => 'Fähigkeiten', 'values' => $this->getSelection($this->typeDefinitions['skills']),
				'min' => 1, 'max' => 5,  'total' => 5 + $char->stats['misc']['skills'] );
		$definition['resources'] = array( 'title' => 'Resourcen', 'values' => $this->getSelection($this->typeDefinitions['resources']),
				'min' => -1, 'max' => 6, 'total' => 2  + $char->stats['misc']['resources']);
		return $definition;
	}

	public function getRasse() {
		$definition = array();
		$definition['attributes'] = array( 'title' => 'Attribute', 'values' => $this->getSelectionNames($this->typeDefinitions['attributes']),
				'min' => -1, 'max' => 2, 'total' => 2 );
		$definition['strengths'] = array( 'title' => 'Stärken', 'values' => $this->getSelectionCosts($this->typeDefinitions['strengths']),
				'min' => 1, 'max' => 1, 'total' => 4, 'costs' => $this->getCosts($this->typeDefinitions['strengths']) );
		return $definition;

	}

	public function getKultur() {
		$definition = array();
		$definition['strengths'] = array( 'title' => 'Stärken', 'values' => $this->getSelectionCosts($this->typeDefinitions['strengths']),
				'min' => 1, 'max' => 1, 'total' => 1, 'costs' => $this->getCosts($this->typeDefinitions['strengths']) );
		$definition['skills'] = array( 'title' => 'Fähigkeiten', 'values' => $this->getSelection($this->typeDefinitions['skills']),
				'min' => 1, 'max' => 2,  'total' => 15, 'total_magic' => 1, 'max_magic' => 1 );
		$definition['masteries'] = array( 'title' => 'Meisterschaften', 'values' => $this->getSelectionMasteries($this->typeDefinitions['masteries']),
				'min' => 1, 'max' => 1, 'total' => 1 );
		return $definition;

	}

	public function getAbstammung() {
		$definition = array();
		$definition['skills'] = array( 'title' => 'Fähigkeiten', 'values' => $this->getSelectionNoMagic($this->typeDefinitions['skills']),
				'min' => 1, 'max' => 1, 'total' => 5 );
		$definition['resources'] = array( 'title' => 'Resourcen', 'values' => $this->getSelection($this->typeDefinitions['resources']),
				'min' => -1, 'max' => 6, 'total' => 4 );
		return $definition;

	}

	public function getAusbildung() {
		$definition = array();
		$definition['strengths'] = array( 'title' => 'Stärken', 'values' => $this->getSelectionCosts($this->typeDefinitions['strengths']),
				'min' => 1, 'max' => 1, 'total' => 2,
				'costs' => $this->getCosts($this->typeDefinitions['strengths']) );
		$definition['skills'] = array( 'title' => 'Fähigkeiten', 'values' => $this->getSelection($this->typeDefinitions['skills']),
				'min' => 1, 'max' => 4, 'total' => 30, 'max_skill' => 3 );
		$definition['resources'] = array( 'title' => 'Resourcen', 'values' => $this->getSelection($this->typeDefinitions['resources']),
				'min' => -1, 'max' => 6, 'total' => 2 );
		$definition['masteries'] = array( 'title' => 'Meisterschaften', 'values' => $this->getSelectionMasteries($this->typeDefinitions['masteries']),
				'min' => 1, 'max' => 1, 'total' => 2 );
		return $definition;
	}
} 
