<?php


class forum_view {
	private $context = array('self' => PAGE_SELF);

	public function createThread(forum_board $board) {
		$this->context['action'] = PAGE_SELF . '&create=' . $board->id;
		$this->context['breadcrumb'] = $board->breadcrumb();
		template('forum/reply')->display($this->context);
	}

	public function reply(forum_thread $thread) {
		$this->context['action'] = PAGE_SELF . '&reply=' . $thread->id;
		$this->context['breadcrumb'] = $thread->board->breadcrumb();
		$this->context['thread'] = $thread;
		$this->context['title'] = 'RE: ' . $thread->title;
		template('forum/reply')->display($this->context);
	}

	public function edit($posting, forum_thread $thread) {
		$this->context['action'] = PAGE_SELF . '&edit=' . $posting['id'];
		$this->context['breadcrumb'] = $thread->board->breadcrumb();
		$this->context['thread'] = $thread;
		$this->context['title'] = $posting['title'];
		$this->context['text'] = $posting['text_raw'];
		template('forum/reply')->display($this->context);
	}

	public function showThread(forum_thread $thread) {
		/** @var rights_container $rights */
		$rights = iv::get('rights');
		$user = iv::get('user');
		$board = $thread->loadBoard();
		$flags = $rights->flags('forum', $board->id);

		if (!$board->public_read && !$rights->has('forum', $board->id)) {
			throw new Exception('Sie haben keinen Zugriff auf dieses Forum!');
		}

		$thread->markRead();

		$pagination = new data_pagination(new html_link(PAGE_SELF.'&thread='.$thread->id), 25, 'thread_' );
		$this->context['postings'] = $pagination->query("SELECT p.*, u.name as user_name, u.id as user_id, u.email as user_email
			FROM forum_post p LEFT JOIN user_data u ON u.id = p.create_by
			WHERE p.thread = %d ORDER BY id", $thread->id)->assocs();

		$this->context['author'] = db()->user_data->row($thread->create_by)->assoc();
		$this->context['breadcrumb'] = $board->breadcrumb();
		$this->context['thread'] = $thread;
		$this->context['reply'] = $user && (!$thread->closed || $flags['moderate']) && ($board->public_reply || $flags['reply']);
		$this->context['moderate'] = $flags['moderate'];
		$this->context['user'] = $user;
		$this->context['pagination'] = $pagination->get();
		template('forum/thread')->display($this->context);
	}

	public function showBoard($boardId) {
		$board = forum_board::load($boardId);

		/** @var rights_container $rights */
		$rights = iv::get('rights');

		if (!$board->public_read && !$rights->has('forum', $board->id)) {
			throw new Exception('Sie haben keinen Zugriff auf dieses Forum!');
		}

		$flags = $rights->flags('forum', $board->id);
		$board->writable = iv::get('user') && ($board->public_write || $flags['write']);
		$board->subs = $board->getBoards();
		$board->threads = $board->getThreads();

		$this->context['breadcrumb'] = $board->breadcrumb();
		$this->context['boards'] = array($board);
		template('forum/boards')->display($this->context);
	}

	public function listBoards() {
		$boards = db()->query("
			SELECT * FROM forum_board
			WHERE parent IS NULL")->objects(null, 'forum_board');

		/** @var rights_container $rights */
		$rights = iv::get('rights');

		foreach ($boards as $i => $board) {
			if (!$board->public_read && !$rights->has('forum', $board->id)) {
				unset($boards[$i]);
				continue;
			}

			$board->subs = $board->getBoards();
			$board->threads = $board->getThreads();
			$board->writable = false;
		}

		$this->context['boards'] = $boards;
		template('forum/boards')->display($this->context);
	}
} 