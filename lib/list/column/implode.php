<?php

class list_column_implode extends list_column {
	protected $glue;

	public function __construct($caption, $data, $glue = ' ') {
		parent::__construct($caption, $data);
		$this->glue = $glue;
	}

	public function cell($row) {
		return is_array( $row[$this->data] ) ? implode( $this->glue, $row[$this->data] ) : '';
	}
}
