<?php

/** @var splitter_character $char */
$char = db()->splitter_character->get("id = %d AND create_by = %d", $_GET['id'], current_user())->object('splitter_character');
if( empty( $char )) throw new Exception('No character found');

if( !empty( $_GET['revert'] )) {
	$changes = db()->query("SELECT * FROM splitter_changes
		WHERE `character` = %d AND id >= %d ORDER BY id DESC", $char->id, $_GET['revert'] )->assocs();

	foreach( $changes as $change )
		$char->stats = applyDiff($char->stats, json_decode($change['diff'], true));

	saveCharacter($char, 'Änderung Rückgängig machen: '.$change['description']);
}

$log = db()->splitter_changes->get("`character` = %d", $char->id )->assocs();

$tbl = new list_array(PAGE_SELF, 'log');
$tbl->text('Kommentar', 'description');
$tbl->date('Datum', 'create_date');
$tbl->add($actions = new list_column_buttons('Aktionen'));
$actions->add(PAGE_SELF.'&id='.$char->id, 'revert', 'Reverte', true);
$tbl->display($log);


