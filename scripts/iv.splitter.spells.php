<?php

require 'inc/load_data.php';

if( $_POST['types']['spells'] ) {
	$spells = array();

	foreach( $_POST['types']['spells'] as $spell ) {
		$id = $spell[0];
		$spells[$id] = $zauber[$id];
	}

	$panel['spells'] = $spells;
}

$values = array();
foreach( $zauber as $id => $z )
	$values[$id] = $id;

$panel['stats'] = array(
	'spells' => array(
		'title' => 'Zauber',
		'min' => 1,
		'max' => 1,
		'values' => $values,
	)
);

