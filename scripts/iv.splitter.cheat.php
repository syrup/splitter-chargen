<?php

require 'inc/load_data.php';

function getStats( $data, $possible ) {
	$result = array();
	foreach( $data as $k => $v )
		if( isset( $possible[$k]) )
			$result[$k] = 1;
	return $result;
}

$schulen = array();
foreach ($zauber as $id => $z)
	foreach ($z['schulen'] as $s => $g)
		$schulen[$s][$id] = $z;
$panel['schulen'] = $schulen;

$panel['spells'] = splitter_definitions::getSelection($zauber);
$panel['strengths'] = $sVals = splitter_definitions::getSelectionCosts($typeDefinitions['strengths']);
$panel['weaknesses'] = $wVals = splitter_definitions::getSelection($wks);
$panel['masteries'] = $mVals = splitter_definitions::getSelectionMasteries($typeDefinitions['masteries']);

/** @var splitter_character $char */
$char = db()->splitter_character->get("id = %d AND create_by = %d", $_GET['id'], current_user())->object('splitter_character');
if (empty($char)) throw new Exception('Ungültiger Character');

if (isset($_POST['stats'])) {
	foreach ($atr as $id => $info) {
		$char->start_attribute[$id]     = intval($_POST['start_attributes'][$id]);
		$char->stats['attributes'][$id] = intval($_POST['stats']['attributes'][$id]);
	}

	foreach ($res as $id => $info)
		$char->stats['resources'][$id] = intval($_POST['stats']['resources'][$id]);
	foreach ($skl as $id => $info)
		$char->stats['skills'][$id] = intval($_POST['stats']['skills'][$id]);
	$char->inventar = $_POST['inventory'];

	$char->stats['strengths'] = getStats($_POST['stats']['strengths'], $sVals);
	$char->stats['weaknesses'] = getStats($_POST['stats']['weaknesses'], $wVals);
	$char->stats['masteries'] = getStats($_POST['stats']['masteries'], $mVals);

	$newSpells = array();
	foreach($_POST['stats']['spells'] as $schule => $spells )
		if( isset( $schulen[$schule] ))
			$newSpells[$schule] = getStats($spells, $panel['spells']);
	$char->stats['spells'] = $newSpells;

	saveCharacter($char, 'Änderungen im Cheat-Modus');
}

$panel['char']        = $char;
$panel['definitions'] = $typeDefinitions;
$panel['charpage'] = db()->query("SELECT layer FROM content_panel WHERE script = 'splitter.character'")->value();
