<?php

global $elementTypes;

if( $_GET['delete'] ) {
	db()->splitter_character->update(
			array('create_by'=>0),
			"id = %d AND create_by = %d",
			$_GET['delete'], current_user()
	);
}

$panel['elementpage'] = $ep = db()->query("
				SELECT layer FROM content_panel WHERE script = 'splitter.element'")->value();
$panel['characterpage'] = $cp = db()->query("
				SELECT layer FROM content_panel WHERE script = 'splitter.character'")->value();

$tbl = new list_sql( PAGE_SELF );
$tbl->text('Name', 'name');
$tbl->class = 'table table-striped';
$tbl->add($actions = new list_column_buttons('Aktionen'));
$actions->add('index.php?page='.$ep, 'id', 'Edit');

foreach( $elementTypes as $t ) {
	$tbl->prefix = $t.'_';
	$panel[$t] = $tbl->get("SELECT * FROM splitter_element WHERE type = '$t' AND create_by = ".current_user());
}

$charTbl = new list_sql( PAGE_SELF );
$charTbl->text('Name', 'name');
$charTbl->add($actions = new list_column_buttons('Aktionen'));
$charTbl->class = 'table table-striped';
$charTbl->prefix = 'char_';
$actions->add('index.php?page='.$cp, 'id', 'Open');
$actions->add('index.php?interface=iv.splitter.pdf', 'id', 'PDF');
$actions->add(PAGE_SELF, 'delete', 'Löschen', true);

$panel['characters'] = $charTbl->get("SELECT * FROM splitter_character WHERE create_by = ".current_user());
