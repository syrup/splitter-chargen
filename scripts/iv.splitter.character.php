<?php

global $elementTypes;
/** @var splitter_character $char */
$char = null;

if( $_GET['id'] )
	$char = db()->splitter_character->get("id = %d AND create_by = %d", $_GET['id'], current_user())->object('splitter_character');

$panel['self'] = $self = PAGE_SELF.'&id='.(isset($char)?$char->id:'');
$panel['captions'] = array(
	'attributes' => 'Attribute',
	'strengths' => 'Stärken',
	'skills' => 'Fähigkeiten',
	'masteries' => 'Meisterschaften',
	'resources' => 'Resourcen',
);

if( empty( $char )) {
	if( !empty( $_POST['name'] )) {
		db()->splitter_character->insert(array( 'name' => $_POST['name'] ));
		throw new redirect($self.db()->id());
	}
} else {
	require 'inc/load_data.php';
	$panel['char'] = $char;
	$panel['step'] = $step = $char->getStep();
	$panel['definitions'] = $typeDefinitions;
	$charStats = $char->stats;

	if( $step < 9 ) {
		$controller = new splitter_controller_create($char);
		switch( $step ) {
			case 8: $data = $controller->finalize($zauber, $mst, $skl); break;
			case 7: $data = $controller->selectMondzeichen($mnz, $wks); break;
			case 6: $data = $controller->selectFree($typeDefinitions); break;
			case 5: $data = $controller->selectAttributes($atr); break;
			default: $data = $controller->selectElement($elementTypes, $step, $typeDefinitions);
		}
	} else {
		// $controller = new splitter_controller_character($char);
		if( !empty( $_POST['ep'] )) {
			$ep = intval($_POST['ep']);
			$char->stats['ep']['total'] += $ep;
			saveCharacter( $char, 'Erfahrungspunkte hinzugefügt: '.$ep );
		}

		$data = array(
			'cheatpage' => $cp = db()->query("SELECT layer FROM content_panel WHERE script = 'splitter.cheat'")->value(),
			'logpage' => $cp = db()->query("SELECT layer FROM content_panel WHERE script = 'splitter.log'")->value(),
		);
	}

	$panel = array_merge($panel, $data);

	foreach( $elementTypes as $t )
		$char->{$t} = json_decode( $char->{$t.'_selections'}, true);
}
