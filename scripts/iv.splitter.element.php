<?php

require 'inc/load_data.php';
$definitions = new splitter_definitions($typeDefinitions);
$elementType = $_GET['type'];

if( !empty( $_GET['id'] )) {
	if( iv::get('rights')->has('modul', 'iv.splitter.elements' ))
		$edit = db()->splitter_element->row( $_GET['id'] )
				->object('splitter_element', array($definition, $typeDefinitions));
	else
		$edit = db()->splitter_element
				->get("id = '%d' AND create_by = %d", $_GET['id'], current_user())
				->object('splitter_element', array($definition, $typeDefinitions));
	$elementType = $edit->type;
}

switch($elementType) {
	// case 'rasse': $definition = $definitions->getRasse(); break;
	case 'herkunft': $definition = $definitions->getKultur(); break;
	case 'abstammung': $definition = $definitions->getAbstammung(); break;
	case 'ausbildung': $definition = $definitions->getAusbildung(); break;
	default: throw new Exception('Invalid Type');
}

try {
	$panel['post'] = $_POST;

	if( !empty( $_POST['name'] )) {
		$types = $_POST['types'];
		$values = $_POST['values'];

		$element = new splitter_element($definition, $typeDefinitions);

		foreach($definition as $type => $data )
			if( is_array( $types[$type] )) {
				foreach( $types[$type] as $k => $row )
					if( !empty( $row ))
						$element->add( $type, $row, $values[$type][$k] );
			}

		$element->validate();
		$data = array(
				'type' => $elementType,
				'name' => $_POST['name'],
				'description' => $_POST['description'],
				'stats' => $element,
				'public' => isset( $_POST['public'] )
		);

		if( empty( $edit )) db()->splitter_element->insert( $data );
		else db()->splitter_element->updateRow( $data, $edit->id );

		if( !empty($_GET['ref'] )) {
			$charPage = db()->query("
				SELECT layer FROM content_panel WHERE script = 'splitter.character'")->value();
			throw new redirect('index.php?page='.$charPage.'&id='.intval($_GET['ref']).'#own-hash');
		}

		throw new redirect('index.php');
	} elseif( !empty( $edit )) {
		$panel['post'] = $edit->getPostValues();
	}
} catch (redirect $e) {
	throw $e;
} catch (Exception $e) {
	echo '<div class="alert alert-error">'.$e->getMessage().'</div>';
}

$panel['stats'] = $definition;
$panel['title'] = ucfirst( $elementType ).(empty( $edit ) ? " anlegen" : " bearbeiten" );
$panel['self'] = PAGE_SELF.'&type='.$_GET['type'].'&ref='.intval($_GET['ref'])."&id=".$edit->id;
