<?php

if( file_exists( 'inc/ftp.config.php' )) {
	require_once 'inc/ftp.config.php';
	$writer = new writer_ftp( $ftp_host, $ftp_user, $ftp_pass, $ftp_dir );
} else {
	$writer = new writer_fs();
}

if( !empty( $_POST )) {
	// Name Validation
	if( empty( $_POST['register_name'] ))
		$error = 'Bitte Namen angeben';
	if( preg_match( '/[^\w\d]/', $_POST['register_name'], $m ))
		$error = 'Der Name enthält ungültige Zeichen: '.htmlspecialchars( $m[0] );

	// Email Validation
	elseif( empty( $_POST['register_mail'] ))
		$error = 'Bitte E-Mail angeben';
	elseif( !$mail = filter_var($_POST['register_mail'], FILTER_VALIDATE_EMAIL))
		$error = 'Die angegebene E-Mail ist ungültig';

	// Password Validation
	elseif( empty( $_POST['register_pass'] ))
		$error = 'Bitte Passwort wählen';
	elseif( $_POST['register_pass'] != $_POST['register_repetition'] )
		$error = 'Passwort und Wiederholung stimmen nicht überein';

	// Do the registration
	else {
		$db->insert('user_data', array(
			'name' => $_POST['register_name'],
			'email' => $_POST['register_mail'],
			'pass_salt' => $salt = uniqid(),
			'pass_hash' => md5( $salt.md5( $_POST['register_pass'].$salt )),
			'type' => 7
		));

		$writer->delete('install.php');
		header('Location: admin.php');
		exit();
	}
}


