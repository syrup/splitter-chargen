<?php

 $sources = $db->query('SELECT * FROM update_server')->assocs('id');

?><!DOCTYPE html>
<html lang="de">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>IV Entertainment Installer</title>

		<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">

		<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>


		<script type="text/javascript" src="<?php echo $server['url']; ?>assets/js/migration.js"></script>
		<script type="text/javascript" src="<?php echo $server['url']; ?>assets/js/update.js"></script>

		<style type="text/css">body { background-color: #e3e9ee }</style>

		<script type="text/javascript">
			var update = new UpdateManager( <?php
				echo json_encode( IV_SELF ).',';
				echo json_encode( $sources );
			?>, [] );

			$( function() {
				update.installPackages( <?php echo json_encode($packages); ?> );
			});
		</script>

		<?php
			foreach( $sources as $source )
				echo '<script type="text/javascript" src="'.$source['url'].'?interface=iv.exchange&serverid='.$source['id'].'"></script>';
		?>
	</head>

	<body>

			<div class="modal">
				<div class="modal-header">
					<h3>Installation</h3>
				</div>

				<div class="modal-body" style="overflow: visible; max-height: none;">

					<p>Total Progress</p>

					<div class="progress progress-striped active">
						<div id="totalpgrs" class="bar" style="width: 0%; transition: width 0.3s ease 0s;"></div>
					</div>


					<p>Current Package</p>

					<div class="progress progress-striped active">
						<div id="pkgpgrs" class="bar" style="width: 0%; transition: width 0.1s ease 0s;"></div>
					</div>

					<p>Output Log</p>

					<pre class="well" style="height: 100px; overflow: auto; font-size: 10px;" id="log"><div></div></pre>

				</div>

				<div class="modal-footer">
					<button id="migbtn" class="btn btn-danger" onclick="alert('this button dont has any function')">Cancel</button>
				</div>
			</div>

	</body>
</html><?php
