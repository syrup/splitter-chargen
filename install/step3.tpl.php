<?php

function formfield( $name, $caption, $value = "", $type = "text" ) {
	if( !empty( $_POST[$name] )) $value = $_POST[$name];

	?><div class="control-group<?php echo ( !empty( $_POST ) && empty( $_POST[$name])) ? ' error' : ''; ?>">
	<label class="control-label" for="<?php echo $name; ?>"><?php echo $caption; ?></label>
	<div class="controls">
		<input type="<?php echo $type; ?>" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $value; ?>">
	</div>
	</div><?php
}

?><!DOCTYPE html>
<html lang="de">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>IV Entertainment Installer</title>

	<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">

	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>

	<style type="text/css">body { background-color: #e3e9ee }</style>
</head>

<body>

<form class="form-horizontal" action="<?php echo IV_SELF; ?>complete" method="post">
	<div class="modal">
		<div class="modal-header">
			<h3>Adminaccount anlegen</h3>
		</div>

		<div class="modal-body" style="overflow: visible; max-height: none;"><?php

			if( $error )
				echo '<div class="alert alert-danger">'.$error.'</div>';

			formfield( 'register_name', 'Admin Name' );
			formfield( 'register_pass', 'Passwort', '', 'password' );
			formfield( 'register_repetition', 'Wiederholung', '', 'password' );
			formfield( 'register_mail', 'E-Mail' );

			?>

		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-primary">Installation abschließen</button>
		</div>
	</div>
</form>
</body>
</html><?php
