<?php

header('Content-Type: application/json; charset=utf-8');
error_reporting(E_ALL^E_NOTICE);
$output = array();



$data = json_decode(file_get_contents('http://splitterwiki.de/w/index.php?title=Spezial:Ask&x=-5B-5BKategorie%3AZauber-5D-5D%2F-3F%3DZauber-23%2F-3FBeteiligte-20Werte%2F-3FHGS%2F-3FHatMagieschuleMitZaubergrad%2F-3FHauptkategorie%2F-3FMagieschule1%2F-3FMagieschule2%2F-3FMagieschule3%2F-3FMutterseite%2F-3FReichweite%2F-3FSeitentitel%2F-3FVaterseiten%2F-3FWirkungsbereich%2F-3FWirkungsdauer%2F-3FWirkungsdauerEinheit%2F-3FWirkungsdauerMitEinheit%2F-3FZauberHatMagieschule%2F-3FZauberart%2F-3FZauberdauer%2F-3FZauberdauerMitEinheit%2F-3FZaubergrad1%2F-3FZaubergrad2%2F-3FZaubergrad3%2F-3FZauberkosten%2F-3FZauberkostenMitVerst%C3%A4rken%2F-3FZauberkostenNurVerst%C3%A4rken%2F-3FZauberoption%2F-3FZauberoptionFlacheListe%2F-3FZauberreichweite%2F-3FZauberschwierigkeit%2F-3FZaubertypus%2F-3FZaubertypusFlacheListe%2F-3FZauberverst%C3%A4rkungWirkung%2F-3FZauberwirkung%2F-3FZuletzt-20ge%C3%A4ndert&format=json&limit=1000&link=none&headers=plain&mainlabel=Zauber&searchlabel=Zauber%20-%20alle%20Zauber%20mit%20allen%20Attributen%20-%20JSON-Format&offset=0'), true);

foreach( $data['results'] as $zauber => $werte ) {
	$w = $werte['printouts'];
	$z = array( 'schulen' => array());

	$z['Dauer'] = $w['ZauberdauerMitEinheit'][0];
	$z['Wirkungsdauer'] = $w['WirkungsdauerMitEinheit'][0];
	$z['Zauberkosten'] = $w['Zauberkosten'][0].' ('.$w['ZauberkostenMitVerstärken'][0].')';
	$z['Optionen'] = $w['ZauberoptionFlacheListe'];
	$z['Reichweite'] = $w['Zauberreichweite'][0];
	$z['Schwierigkeit'] = $w['Zauberschwierigkeit'][0];
	$z['Typus'] = $w['ZaubertypusFlacheListe'][0];
	$z['Wirkung'] = $w['Zauberwirkung'][0];
	$z['Verstärkt'] = $w['ZauberverstärkungWirkung'][0];

	for( $i = 1; $i < 4; $i++ )
		if( !empty( $w['Magieschule'.$i] )) {
			$schule = $w['Magieschule'.$i][0]['fulltext'];
			$grad = $w['Zaubergrad'.$i][0];
			$z['schulen'][$schule] = $grad;
		}

	$output[$zauber] = $z;
}

/*

$spells = array(
		'/wiki/Alarm' => 'Alarm',
		'/wiki/Allverst%C3%A4ndnis' => 'Allverständnis',
		'/wiki/Aura_der_Entschlossenheit' => 'Aura der Entschlossenheit',
		'/wiki/Aura_der_Kontramagie' => 'Aura der Kontramagie',
		'/wiki/Aura_der_Lebenskraft' => 'Aura der Lebenskraft',
		'/wiki/Aura_der_Schatten' => 'Aura der Schatten',
		'/wiki/Aura_der_Schmerzlosigkeit' => 'Aura der Schmerzlosigkeit',
		'/wiki/Aura_der_W%C3%A4rme' => 'Aura der Wärme',
		'/wiki/Aura_des_Anf%C3%BChrers' => 'Aura des Anführers',
		'/wiki/Ausdauer_st%C3%A4rken' => 'Ausdauer stärken',
		'/wiki/Befl%C3%BCgelte_Waffe' => 'Beflügelte Waffe',
		'/wiki/Beherrschung_aufheben' => 'Beherrschung aufheben',
		'/wiki/Beschleunigen' => 'Beschleunigen',
		'/wiki/Bestienform' => 'Bestienform',
		'/wiki/Blenden' => 'Blenden',
		'/wiki/Blick_in_die_Sonne' => 'Blick in die Sonne',
		'/wiki/Blitze_rufen' => 'Blitze rufen',
		'/wiki/Blitzschlag' => 'Blitzschlag',
		'/wiki/B%C3%A4renst%C3%A4rke' => 'Bärenstärke',
		'/wiki/Cham%C3%A4leon' => 'Chamäleon',
		'/wiki/Dunkelheit' => 'Dunkelheit',
		'/wiki/Durch_W%C3%A4nde_gehen' => 'Durch Wände gehen',
		'/wiki/Einfrieren' => 'Einfrieren',
		'/wiki/Eins_mit_dem_Feuer' => 'Eins mit dem Feuer',
		'/wiki/Einstellung_ersp%C3%BCren' => 'Einstellung erspüren',
		'/wiki/Einstellung_verbessern' => 'Einstellung verbessern',
		'/wiki/Ein%C3%A4schern' => 'Einäschern',
		'/wiki/Eiserne_Aura' => 'Eiserne Aura',
		'/wiki/Ende_des_Weges' => 'Ende des Weges',
		'/wiki/Feuerstrahl' => 'Feuerstrahl',
		'/wiki/Flamme' => 'Flamme',
		'/wiki/Flammende_Waffe' => 'Flammende Waffe',
		'/wiki/Flammenherrschaft' => 'Flammenherrschaft',
		'/wiki/Fluch_der_Schmerzen' => 'Fluch der Schmerzen',
		'/wiki/Freund_der_Tiere' => 'Freund der Tiere',
		'/wiki/Gegenstand_besch%C3%A4digen' => 'Gegenstand beschädigen',
		'/wiki/Geh_noch_nicht' => 'Geh noch nicht',
		'/wiki/Geisterblick' => 'Geisterblick',
		'/wiki/Geisterdolch' => 'Geisterdolch',
		'/wiki/Geistersprache' => 'Geistersprache',
		'/wiki/Hauch_der_Geister' => 'Hauch der Geister',
		'/wiki/H%C3%A4rte' => 'Härte',
		'/wiki/Katzenreflexe' => 'Katzenreflexe',
		'/wiki/Kleiner_Magieschutz' => 'Kleiner Magieschutz',
		'/wiki/Kleintierform' => 'Kleintierform',
		'/wiki/Kometenwurf' => 'Kometenwurf',
		'/wiki/Krallen' => 'Krallen',
		'/wiki/Lebensband' => 'Lebensband',
		'/wiki/Magische_R%C3%BCstung' => 'Magische Rüstung',
		'/wiki/Magischer_Schlag' => 'Magischer Schlag',
		'/wiki/Nebelsicht' => 'Nebelsicht',
		'/wiki/Rankenpfeil' => 'Rankenpfeil',
		'/wiki/Rindenhaut' => 'Rindenhaut',
		'/wiki/Sanfter_Fall' => 'Sanfter Fall',
		'/wiki/Schattenmantel' => 'Schattenmantel',
		'/wiki/Schattenpfeil' => 'Schattenpfeil',
		'/wiki/Schattenschleier' => 'Schattenschleier',
		'/wiki/Schattenspiel' => 'Schattenspiel',
		'/wiki/Schutz_vor_Feuer' => 'Schutz vor Feuer',
		'/wiki/Tiersinne' => 'Tiersinne',
		'/wiki/Wetterkontrolle' => 'Wetterkontrolle',
		'/wiki/Widerst%C3%A4nde_erh%C3%B6hen' => 'Widerstände erhöhen',
		'/wiki/Wiederherstellung' => 'Wiederherstellung',
		'/wiki/Willenloser_Diener' => 'Willenloser Diener',
		'/wiki/Windschild' => 'Windschild',
		'/wiki/Windsto%C3%9F' => 'Windstoß',
		'/wiki/Wolkenpfad' => 'Wolkenpfad',
		'/wiki/Zauber_bannen' => 'Zauber bannen',
		'/wiki/Zauber_unterdr%C3%BCcken' => 'Zauber unterdrücken',
		'/wiki/Zauber_zur%C3%BCckwerfen' => 'Zauber zurückwerfen',
		'/wiki/Zauberer_behindern' => 'Zauberer behindern',
		'/wiki/Zauberfeder' => 'Zauberfeder',
		'/wiki/Zone_der_Bannung' => 'Zone der Bannung',
		'/wiki/Zufall_bannen' => 'Zufall bannen',
		'/wiki/Zunge_des_Diplomaten' => 'Zunge des Diplomaten',
);

$base = 'http://splitterwiki.de';

foreach( $spells as $url => $name ) {
	$page = file_get_contents($base.$url);

	if( preg_match( '|<table class="regelbox">(.*?)</table>|is', $page, $matches )) {
		$spell = array();
		$source = str_replace("\n", "", $matches[1]);
		$source = str_replace("\r", "", $source);

		preg_match_all('|<tr><td>\s?([^<]+)\s?</td><td>(.*?)</td></tr>|is', $source, $data, PREG_SET_ORDER );

		foreach( $data as $match ) {
			$spell[$match[1]] = strip_tags( $match[2] );
		}

		$output[$name] = $spell;
	}
}

*/

echo str_replace( '&nbsp;', ' ', json_encode($output, JSON_PRETTY_PRINT));
