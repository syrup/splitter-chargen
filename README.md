# README #

Dies soll ein Tool zur Charactererschaffung im Rollenspielsystem Splittermond werden. Das Ganze basiert auf dem IV-CMS 5 und ist zur Zeit noch in Arbeit.

* Splittermond Website: http://splittermond.de
* Forenthread: http://forum.splittermond.de/index.php?topic=384.90

Bibliotheken

* http://api.jquery.com/
* http://getbootstrap.com/2.3.2/
* http://silviomoreto.github.io/bootstrap-select/
* http://twig.sensiolabs.org/documentation
* https://github.com/geersch/bootstrap-spinedit

### Installation aus dem Repository ###

Um das System zum laufen zu bringen, muss man nach dem checkout noch folgende schritte befolgen:

1. die Datei inc/mysql.example.php kopieren, mit den Zugangsdaten der DB versehen und in inc/mysql.config.php umbenennen
2. die setup.php einmal in der Konsole ausführen
3. über phpMyAdmin oder ein ähnliches Werkzeug einen User anlegen, das Passwort muss bei leerem salt doppelt mit md5 verschlüsselt werden. Der Type sollte wie beim alten System 7 entsprechen
4. einen Ordner "cache" erstellen mit Rechten 777

Wer über keinen SSH-Zugang verfügt findet zu Schritt 2 hier noch ein paar tipps:
http://iv-cms.de/Forum?&thread=55