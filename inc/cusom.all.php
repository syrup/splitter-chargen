<?php

$conf->page->supress_rewrite = true;
$elementTypes = array( 'rasse', 'herkunft', 'abstammung', 'ausbildung' );
$captions = array(
		'attributes' => 'Attribute',
		'strengths' => 'Stärken',
		'skills' => 'Fähigkeiten',
		'masteries' => 'Meisterschaften',
		'resources' => 'Resourcen',
);

function replaceSpecialchars($str) {
	return str_replace(
			array(' & ', ' '),
			array('&', '_'),
			$str);
}

function replaceUmlaute($str) {
	return str_replace(
			array('ä', 'ö', 'ü', 'Ä', 'Ö', 'Ü', 'ß', 'Geistiger  Widerstand  des  Ziels', 'Koerperlicher Widerstand des  Ziels', 'Verteidigung des Ziels'),
			array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss', 'GW', 'KW', 'VTD'),
			$str);
}

function saveCharacter( splitter_character $char, $log = false ) {
	$data = (array) $char;
	$data['stats'] = json_encode( $char->stats );
	$data['start_attribute'] = json_encode( $char->start_attribute );
	db()->splitter_character->updateRow( $data, $char->id );

	if( $log ) {
		db()->splitter_changes->insert(array(
				'character' => $char->id,
				'description' => $log,
				'diff' => json_encode($char->getDiff())
		));
	}

	throw new redirect( PAGE_SELF.'&id='.$char->id );
}

function getDiff( $original, $changed ) {
	$diff = array();
	$keys = array_keys( array_merge( $original, $changed ));

	foreach( $keys as $key )
		if( is_array($original[$key]) && is_array( $changed[$key])) {
			if( $subDiff = getDiff( $original[$key], $changed[$key] ))
				$diff[$key] = $subDiff;
		} elseif( $original[$key] != $changed[$key] ) {
			$diff[$key] = empty( $original[$key] ) ? 0 : $original[$key];
		}

	return $diff;
}

function applyDiff( $data, $diff ) {
	$result = array();
	$keys = array_keys( array_merge( $data, $diff ));

	foreach( $keys as $key )
		if( !isset( $diff[$key] )) {
			$result[$key] = $data[$key];
		} elseif( is_array( $data[$key] ) && !empty( $diff[$key] )) {
			$result[$key] = applyDiff( $data[$key], $diff[$key] );
		} elseif( !empty( $diff[$key] )) {
			$result[$key] = $diff[$key];
		}

	return $result;
}
