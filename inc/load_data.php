<?php

$mst = json_decode(file_get_contents( 'data/meisterschaften.json' ), true );

foreach( $mst as $k => $schwellen )
	foreach( $schwellen as $i => $schwelle )
		foreach( $schwelle as $m => $s )
			if( 'Schwerpunkte' == $i  )
				$mst[$k][$i][$m] = array(
						'name' => $k.': Schwerpunkt '.$s,
						'id' => $k.': Schwerpunkt '.$s,
				);
			else $mst[$k][$i][$m]['id'] =  $k.': '.$m;

// cache this shit? speed is not jet a problem ;-)
$typeDefinitions = array(
		'attributes' => $atr = json_decode(file_get_contents( 'data/attribute.json' ), true ),
		'strengths' => $str = json_decode(file_get_contents( 'data/staerken.json' ), true ),
		'skills' => $skl = json_decode(file_get_contents( 'data/fertigkeiten.json' ), true ),
		'masteries' => $mst,
		'resources' => $res = json_decode(file_get_contents( 'data/resourcen.json' ), true ),
		'weaknesses' => $wks = json_decode(file_get_contents( 'data/schwaechen.json' ), true ),
		'mondzeichen' => $mnz = json_decode(file_get_contents( 'data/mondzeichen.json' ), true ),
		'spells' => $zauber = json_decode(file_get_contents( 'data/zauber.json' ), true )
);

