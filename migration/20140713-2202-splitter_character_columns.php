<?php

function install() {
	db()->query("ALTER TABLE `splitter_element` ADD INDEX ( `type` );");
	db()->query("ALTER TABLE `splitter_character` ADD `rasse_selections` TEXT NOT NULL AFTER `rasse`;");
	db()->query("ALTER TABLE `splitter_character` ADD `herkunft_selections` TEXT NOT NULL AFTER `herkunft`;");
	db()->query("ALTER TABLE `splitter_character` ADD `abstammung_selections` TEXT NOT NULL AFTER `abstammung`;");
	db()->query("ALTER TABLE `splitter_character` ADD `ausbildung_selections` TEXT NOT NULL AFTER `ausbildung`;");
	db()->query("ALTER TABLE `splitter_character` ADD `start_attribute` TEXT NOT NULL AFTER `ausbildung_selections`;");
	db()->query("ALTER TABLE `splitter_character` ADD `abrunden` TEXT NOT NULL AFTER `start_attribute`;");
}

function remove() {
	db()->query("");
}
