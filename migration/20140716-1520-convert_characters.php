<?php

function install() {
	global $elementTypes;
	require 'inc/load_data.php';

	foreach( db()->splitter_character as $char ) {
		$update = array();

		foreach( $elementTypes as $t )
			if( $char[$t] ) {
				$element = db()->splitter_element->row( $char[$t] )->object('splitter_element', array(array(), $typeDefinitions));
				$stats = $element->select( json_decode( $char[$t.'_selections'], true));
				$stats['name'] = $element->name;
				$update[$t.'_selections'] = json_encode($stats);
			}

		db()->splitter_character->updateRow($update, $char['id']);
	}
}

function remove() {
	db()->query("");
}
