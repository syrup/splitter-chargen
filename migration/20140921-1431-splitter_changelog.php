<?php

function install() {
	foreach(db()->splitter_character as $char) {
		$stats = json_decode($char['stats'], true);

		if( !empty( $stats['weaknesses'] )) {
			$weaknesses = array();
			foreach( $stats['weaknesses'] as $w )
				$weaknesses[$w] = 1;
			$stats['weaknesses'] = $weaknesses;

		}

		if( !empty( $stats['spells'] )) {
			$schulen = array();
			foreach( $stats['spells'] as $schule => $spells )
				foreach( $spells as $s )
					$schulen[$schule][$s] = 1;
			$stats['spells'] = $schulen;
		}

		$json = json_encode($stats);
		if($json != $char['stats'])
			db()->splitter_character->updateRow(array(
					'stats' => json_encode($stats)
			), $char['id'] );
	}

	db()->query("CREATE TABLE IF NOT EXISTS `splitter_changes` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `character` int(10) unsigned NOT NULL,
		  `description` varchar(250) NOT NULL,
		  `diff` text NOT NULL,
		  `create_date` int(10) unsigned NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
}

function remove() {
	db()->query("DROP TABLE `splitter_changes`;");
}
