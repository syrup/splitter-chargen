<?php

function install() {
	db()->query("ALTER TABLE `splitter_character` DROP `abrunden`;");
	db()->query("ALTER TABLE `splitter_character` ADD `free_selections` TEXT NOT NULL AFTER `start_attribute`;");
	db()->query("ALTER TABLE `splitter_character` ADD `mondzeichen` TEXT NOT NULL AFTER `free_selections`;");
}

function remove() {
	db()->query("");
}
