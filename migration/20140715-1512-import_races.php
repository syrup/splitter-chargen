<?php

function install() {
	$data = json_decode(file_get_contents('data/import/rassen.json'),true);
	foreach( $data as $i => $race )
		db()->splitter_element->insert(array(
			'id' => $i+1,
			'type' => 'rasse',
			'name' => $race['name'],
			'description' => $race['description'],
			'stats' => json_encode( $race['stats'] )
		), 'REPLACE');
}

function remove() {
	db()->query("");
}
