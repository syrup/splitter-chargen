<?php

function install() {
	db()->query("
		CREATE TABLE IF NOT EXISTS `splitter_element` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `type` enum('rasse','herkunft','abstammung','ausbildung') NOT NULL,
		  `name` varchar(250) NOT NULL,
		  `description` text NOT NULL,
		  `stats` text NOT NULL,
		  `create_by` int(10) unsigned NOT NULL,
		  `public` tinyint(1) NOT NULL,
		  PRIMARY KEY (`id`),
		  KEY `create_by` (`create_by`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

	db()->query("
		CREATE TABLE IF NOT EXISTS `splitter_character` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(250) NOT NULL,
		  `create_by` int(10) unsigned NOT NULL,
		  `rasse` int(10) unsigned NOT NULL,
		  `herkunft` int(10) unsigned NOT NULL,
		  `abstammung` int(10) unsigned NOT NULL,
		  `ausbildung` int(10) unsigned NOT NULL,
		  `stats` text NOT NULL,
		  PRIMARY KEY (`id`),
		  KEY `create_by` (`create_by`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
}

function remove() {
	db()->query("DROP TABLE `splitter_element`;");
	db()->query("DROP TABLE `splitter_character`;");
}
